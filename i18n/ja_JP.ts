<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Notepad</source>
        <translation>メモ帳</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="32"/>
        <source>Please choose language</source>
        <translation>言語を選択してください</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="55"/>
        <source>&amp;File</source>
        <translation>ファイル(&amp;F)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <source>&amp;Edit</source>
        <translation>編集(&amp;E)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <source>&amp;Language</source>
        <translation>言語(&amp;L)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="99"/>
        <source>&amp;Open</source>
        <translation>開く(&amp;O)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="104"/>
        <source>&amp;Close</source>
        <translation>閉じる(&amp;C)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="113"/>
        <source>&amp;New</source>
        <translation>新規作成(&amp;N)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="124"/>
        <source>Cu&amp;t</source>
        <translation>切り取り(&amp;T)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <source>&amp;Copy</source>
        <translation>コピー(&amp;C)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="140"/>
        <source>&amp;Paste</source>
        <translation>貼り付け(&amp;P)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="148"/>
        <source>&amp;Save</source>
        <translation>保存(&amp;S)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="156"/>
        <source>&amp;Chinese</source>
        <translation>中文(&amp;C)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="161"/>
        <source>&amp;English</source>
        <translation>&amp;English</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="166"/>
        <source>&amp;Japanese</source>
        <translation>日本語(&amp;J)</translation>
    </message>
</context>
</TS>
